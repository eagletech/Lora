"""
Sivas Zeka:

Öncelikler ve ağırlıklar:
    Bulunulan konuma yakınlık (aynı şehir)
    Mekan türü (İbadethane, Müze)
    Popülarite (rising, trending)
    Kullanıcıların mekanı beğenme oranı (all time rating)
    Pahalılık

1- Yakınlık:    100
        3km'ye kadar 100-80:    100-(20/3*x)
        3-30km'ye kadar 100-60: 100-(40/30*x)
        30 sonrası:             1.042^x

2- Tanınırlık:  50
3- Puanlama:    35
4- Kalabalık:   30
5- Pahalılık:   20


"""

from time import sleep
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.properties import ObjectProperty

import json
import os
import operator
from re import I
import random

def is_same(text1 : str, text2 : str) -> bool:
    """
returns True in the case of text2 is included in text1 when case-insensitevely compared.
    """
    
    if text1.lower().find(text2.lower()) >= 0:
        return True

    return False

class Koordinat:
    lat = 0.0
    lon = 0.0
    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon

class MekanSarici:
    def __init__(self, mekan):
        self.mekan = mekan[0],
        #print("self.mekan: ", self.mekan)
    def __lt__(self, diger):
        return self.mekan["Ağırlık"] < diger["Ağırlık"]

class Veritabani:
    mekanlar: dict = {}
    mekanlar_gecici: list = []
    agirliklar = []
    konum_lat = 40.9063819
    konum_lon = 29.2109187

    def sec_mekan(self, mekan_adi):
        for sehir, mekanlar in self.mekanlar.items():
            if mekanlar != None:
                for mekan in mekanlar:
                    try:
                        if is_same(mekan["Mekan Adı"], mekan_adi):
                            return mekan
                    except:
                        pass
        return None

    def koordinat_bul(self, mekan_adi):
        mekan = self.sec_mekan(mekan_adi)
        #print(mekan)
        _koord = mekan["Koordinat"].rsplit(sep=",")
        koordinat = Koordinat(float(_koord[0]), float(_koord[1]))
        return koordinat

    def hesapla_yakinlik(self, mekan):
        from math import sin, cos, sqrt, atan2, radians

        dunya_yaricap = 6369 # dünyanın merkezinin sivasa olan uzaklığını dünyanın yarıçapı olarak
                             # kullanıyoruz.

        lat1, lon1 = self.konum_lat, self.konum_lon

        mekan_konum = mekan["Koordinat"]
        _koord = mekan_konum.rsplit(sep=",")
        koordinat = Koordinat(float(_koord[0]), float(_koord[1]))

        lat2, lon2 = koordinat.lat, koordinat.lon

        dLat = radians(lat2-lat1)
        dLon = radians(lon2-lon1)

        lat1 = radians(lat1)
        lat2 = radians(lat2) 

        a = sin(dLat/2) * sin(dLat/2) + \
              sin(dLon/2) * sin(dLon/2) * cos(lat1) * cos(lat2)
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        mesafe = dunya_yaricap * c

        if mesafe <= 3: return 100-(20/3*mesafe)
        elif mesafe <= 30: return 100-(40/30*mesafe)
        else:
            puan_kirici = pow(1.042, mesafe)
            if puan_kirici > 100: return 0
            else: return 100-puan_kirici

    def hesapla_taninirlik(self, taninirlik):
        return taninirlik * 10

    def hesapla_puanlama(self, puan):
        return puan * 10

    def hesapla_kalabalik(self,kalabalik):
        return 10 * kalabalik

    def hesapla_pahalilik(self, pahalilik):
        if pahalilik <= 1: return 0
        else: return (pahalilik-1)*-25

    def hesapla_agirlik(self, mekan):
        try:
            agirlik =   self.hesapla_yakinlik(mekan) + \
                        self.hesapla_taninirlik(mekan["Tanınırlık"]) + \
                        self.hesapla_puanlama(mekan["Puanlaması"]) + \
                        self.hesapla_kalabalik(mekan["Kalabalıklığı"]) + \
                        self.hesapla_pahalilik(mekan["Pahalılığı"]) 
        except:
            agirlik = 0; 

        return agirlik
    
    def oner_mekan(self, mekan_adi1, mekan_adi2, mekan_adi3, mekan_adi4, mekan_adi5, konum):
        mekanlar = [
                   self.sec_mekan(mekan_adi1), \
                   self.sec_mekan(mekan_adi2), \
                   self.sec_mekan(mekan_adi3), \
                   self.sec_mekan(mekan_adi4), \
                   self.sec_mekan(mekan_adi5) \
        ]
        mekan_turleri = []
        for mekan in mekanlar:
            try:
                mekan_turleri.append(mekan["Kategori"])
            except:
                pass

        self.mekanlar_gecici = []
        for sehir, mekan in self.mekanlar.items():
            if mekan != None:
                if is_same(sehir, konum) or konum == "":
                    same_flag = 0
                    for m in mekanlar:

                        if m["Mekan Adı"] == mekan[0]["Mekan Adı"]:
                            same_flag = 1
                            break

                    if not same_flag:
                        agirlik = 0
                        self.mekanlar_gecici += mekan


        self.agirliklar = []
        for mekan in self.mekanlar_gecici:
            agirlik = v.hesapla_agirlik(mekan)
            for tur in mekan_turleri:
                if mekan["Kategori"] == tur:
                    agirlik += 35
            mekan = mekan | {"Ağırlık": agirlik}
            #print(mekan, "\n\n\n")
            self.agirliklar.append(mekan)
        #print(self.agirliklar)
        sonuc = sorted(self.agirliklar, key=lambda a: a["Ağırlık"], reverse=True)
        return sonuc[:10]




v = Veritabani()

# f =  open("main.json")
# data = json.load(f)
#
# for s in data:
#     v.mekanlar[s] = None

# bütün mekanları import eder.
counter = 0
for dosya in os.listdir("db"):
    with open(os.path.join("db", dosya), encoding="utf-8") as d:
        veri: dict = json.load(d)
        for sehir, mekanlar in veri.items():
            try:
                v.mekanlar[sehir] = v.mekanlar[sehir] + mekanlar
            except:
                v.mekanlar[sehir] = mekanlar

terchiler = []

# # testler
# kanyon = v.sec_mekan("Ulubey Kanyonu")
# u = "Ulubey Kanyonu"
# print(kanyon)
# print("\n\n\n")
# sonuc = v.oner_mekan(u,u,u,u,u,u)
# print(len(sonuc))
# print(sonuc)
# #for sehir, mekanlar in v.sehirler.items():
# #    #print(mekanlar)
#
# # #print( v.hesapla_yakinlik(Koordinat(40.9063819,29.2109187), Koordinat(40.9123506,29.192907)) )
# # kartal metro - soğanlık metro koordinatları


class FirstWindow(Screen):
    pass
class SecondWindow(Screen):
    def get_vals(self):
        return self.ids.konum.text, [
                self.ids.gezilen_mekan1.text,
                self.ids.gezilen_mekan2.text, 
                self.ids.gezilen_mekan3.text, 
                self.ids.gezilen_mekan4.text, 
                self.ids.gezilen_mekan5.text, 
            ]
    def display_mekanlar(self, mekanlar):
        self.ids.mekan1.text = mekanlar[0]["Mekan Adı"]
        self.ids.mekan2.text = mekanlar[1]["Mekan Adı"]
        self.ids.mekan3.text = mekanlar[2]["Mekan Adı"]
        self.ids.mekan4.text = mekanlar[3]["Mekan Adı"]
        self.ids.mekan5.text = mekanlar[4]["Mekan Adı"]

    oner = ObjectProperty(None)
    def onerpressed(self):
        konum, vals = self.get_vals()
        mekanlar = v.oner_mekan(*vals, konum)

        print(len(mekanlar))
        self.display_mekanlar(random.sample(mekanlar, 5))


class ThirdWindow(Screen):

    spinnerdil = ObjectProperty(None)
    spinnermekan = ObjectProperty(None)
    spinnerbackground = ObjectProperty(None)
    def dilpressed(self):
        spinnerdil = self.spinnerdil.text
        print(spinnerdil)
    def mekanpressed(self):
        spinnermekan = self.spinnermekan
    def backgroundpressed(self):
        spinnerbackground = self.spinnerbackground
    def spinnerdil_clicked(self, value):
        pass
    def spinnerbackground_clicked(self, value):
        pass

class FourthWindow(Screen):
    pass

class WindowMindowManagerOlan(ScreenManager):
    pass

kv = Builder.load_file('arayuz.kv')

class app(App):
    def build(self):
        #Window.clearcolor=(1, 0, 0, 1)
        return kv

app().run()

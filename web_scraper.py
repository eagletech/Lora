#Kütüphaneler dosya içine alınıyor
from django.forms import EmailField
import requests
from bs4 import BeautifulSoup
import json
import time
from pytrends.request import TrendReq
import pandas as pd

pytrends = TrendReq(hl='tr-TR', tz=120)

json_obj = {}#Tüm iller burada toplanıp main.json dosyasına yazılacak

############İl adları ile illerin objelerinin oluşturulması ve ardından alan, nüfus ve plaka kodu bilgilerinin girilimi############
html = requests.get("https://tr.wikipedia.org/wiki/T%C3%BCrkiye%27nin_illeri").content
soup=BeautifulSoup(html,"html.parser")
il_liste = soup.find_all("table")[4].find_all("tr")
il_liste.remove(il_liste[0])
for tr in il_liste:
    json_obj[tr.find_all("td")[0].text[:-1]] = {
        "bolge" : "",
        "ilceler" : [],
        "alan" : tr.find_all("td")[1].text[:-1] + "km^2",
        "nufus" : tr.find_all("td")[2].text[:-1],
        "kişi/km^2" : tr.find_all("td")[3].text[:-1],
        "plaka_kodu" : tr.find_all("td")[4].text[:-1],
        "popularite" : "",
        "trendlik": "",
        "gelismislik_duzeyi" : "",
        "mekan_sayisi" : {},
        "pahalilik" : "",
        "ortalama_sicaklik" : "",
        "koordinat" : [],
        "ilce" : "",
        "belediye" : "",
        "koy" : ""
    }
###################################################################################################################################


######################################################İlllere  ilçeler ekleniyor###################################################
html = requests.get("https://tr.wikipedia.org/wiki/T%C3%BCrkiye%27nin_il%C3%A7eleri").content
soup=BeautifulSoup(html,"html.parser")
ilce_list = soup.find_all("div",{"class" : "div-col"})
counter = 0
for obj in json_obj:
    ilcelist = ilce_list[counter].ul.find_all("li")
    for ilce in ilcelist:
        json_obj[obj]["ilceler"].append(ilce.a.text)
    counter += 1
###################################################################################################################################


#####################################################İllere bölgeleri ekleniyor####################################################
html = requests.get("https://github.com/yigith/TurkiyeSehirlerBolgeler").content
soup=BeautifulSoup(html,"html.parser")
bolgeler = soup.find_all("tbody")[0].find_all("tr")
counter = 0
for obj in json_obj:
    json_obj[obj]["bolge"] = bolgeler[counter].find_all("td")[4].text
    counter += 1
###################################################################################################################################


###################################################İllere koordinatları ekleniyor##################################################
html = requests.get("https://www.odevde.com/illerin-koordinatlari/").content
soup=BeautifulSoup(html,"html.parser")
koordinatlar = soup.find("tbody").find_all("tr")[1:]
counter = 0
for obj in json_obj:
    json_obj[obj]["koordinat"] = [koordinatlar[counter].find_all("td")[1].text, koordinatlar[counter].find_all("td")[2].text]
    counter += 1
###################################################################################################################################


##################################################İllere gelismislikleri ekleniyor#################################################
html = requests.get("https://tr.wikipedia.org/wiki/T%C3%BCrkiye%27deki_illerin_geli%C5%9Fmi%C5%9Flik_d%C3%BCzeyleri").content
soup=BeautifulSoup(html,"html.parser")
trs = soup.find("tbody").find_all("tr")[1:]
for tr in trs:
    json_obj[tr.find_all("td")[0].text.replace(u"\n", u"")]["gelismislik_duzeyi"] = tr.find_all("td")[1].text.replace(u"\n", u"")
###################################################################################################################################


#####################################################İllere yemekleri ekleniyor####################################################
""" html = requests.get("https://www.ensonhaber.com/galeri/81-ilin-yoresel-yemekleri").content
soup=BeautifulSoup(html,"html.parser")
pyle = soup.find("div", {"class" : "content"}).find_all("div", {"class" : "item"})[2:]
for ple in pyle:
    a = ple.find("div", {"class" : "item-header"}).find("p").text
    il = a[:a.index(" ")].capitalize().replace("i̇", "i").replace("İ", "İ")
    a = a[a.index(" ")+1:]
    try:
        if (il == "Balikesir"): il = "Balıkesir"
        elif (il == "Diyarbakir"): il = "Diyarbakır"
        elif (il == "Hakkari"): il = "Hakkâri"
        elif (il == "Kirklareli"): il = "Kırklareli"
        elif (il == "Kirşehir"): il = "Kırşehir"
        elif (il == "Afyon"): il = "Afyonkarahisar"
        elif (il == "Adiyaman"): il = "Adıyaman"
    except:
        il = il.replace("i", "ı")
    counter = [0, a.count(",")]
    listy=[]
    b = a.index(",")
    while counter[0] <= counter[1]:
        if (counter[0] == counter[1]):
            listy.append(a[:-1])
        else:
            listy.append(a[:b])
            a = a[b+2:]
        counter[0] += 1
    json_obj[il]["meshur_gidalar"] = listy """
###################################################################################################################################


####################################################İllere Sıcaklıkları ekleniyor##################################################
html = requests.get("https://pratiktarim.com/yasadigim-ilin-yillik-ortalama-sicaklik-yillik-ortalama-toprak-sicakligi-ve-yillik-ortalama-yagis-miktarini-ogrenebilir-miyim/").content
soup=BeautifulSoup(html,"html.parser")
sıcaklık_hd = soup.find("tbody").find_all("tr")[1:]
sıcaklık_h = []
for sıcaklık_o in sıcaklık_hd:
    if sıcaklık_o.find_all("td")[0].text != "İl ve/ veya İlçe Adı":
        sıcaklık_h.append(sıcaklık_o)

il_liste_c = []
il_liste = []
for i in range(81):
    il_liste_c.append([])
for obj in json_obj:
    il_liste.append(obj)

for sıcaklık_o in sıcaklık_h[:-1]:
    liste = sıcaklık_o.find_all("td")
    try:
        liste[0].text.index("(")
    except:
        a = liste[0].text
        if (a == "Adapazarı"):
            a = "Sakarya"
        elif (a == "Afyon"):
            a = "Afyonkarahisar"
        elif (a == "Yozga"):
            a = "Yozgat"
        elif (a == "Osmaniye*"):
            a = "Osmaniye"
        elif (a == "Şırnak*"):
            a = "Şırnak"
    else:
        try:
            a = liste[0].text[liste[0].text.index("(")+1:liste[0].text.index(")")].capitalize().replace("i̇", "i").replace("İ", "İ")
        except:
            a = "Ordu"
            continue
        try:
            if (a == "Balikesir"): a = "Balıkesir"
            elif (a == "Diyarbakir"): a = "Diyarbakır"
            elif (a == "Kirklareli"): a = "Kırklareli"
            elif (a == "Kirşehir"): a = "Kırşehir"
            elif (a == "Şanliurfa"): a = "Şanlıurfa"
            elif (a == "Erdemli-mersin"): a = "Mersin"
            elif (a == "Topraksu"): a = "Ankara"
            elif (a == "Çanakkkale"): a = "Çanakkale"
            elif (a == "Şirnak"): a = "Şırnak"
            elif (a == "Iğdir"): a = "Iğdır"
            elif (a == "Afyon"): a = "Afyonkarahisar"
            elif (a == "Ağri"): a = "Ağrı"
            elif (a == "Elaziğ"): a = "Elazığ"
            elif (a == "Yeşilovaaksaray"): a = "Aksaray"
            elif (a == "Aydin"): a = "Aydın"
            elif (a == "Tarım işletmesi"): a = "Ankara"
            elif (a == "Gönen – balikesir "): a = "Balıkesir"
            elif (a == "Adiyaman"): a = "Adıyaman"
            elif (a == "Çankiri"): a = "Çankırı"
            elif (a == "Kirikkale"): a = "Kırıkkale"
            elif (a == " bolu"): a = "Bolu"
        except: a = a.replace("i", "ı")
    finally:
        if a == "Hakkari":
            a = "Hakkâri"
        il_liste_c[il_liste.index(a)].append(liste[1].text.replace(u'\xa0', u''))
sıcaklıklar = []
for il in il_liste_c:
    a = 0
    counter = 0
    for i in il:
        if i == "12,,5":
            i = "12,5"
        if i == ",":
            i = "0"
            counter -= 1
        a += float(i.replace(",", "."))
        counter += 1
    sıcaklıklar.append(a/counter)

counter = 0
for obj in json_obj:
    json_obj[obj]["ortalama_sicaklik"] = str(sıcaklıklar[counter])
    counter += 1
###################################################################################################################################


####################################################İllere trendlikleri ekleniyor##################################################
pytrends.build_payload(["İstanbul"], cat=0, timeframe='today 12-m', geo='TR')
dgrs = pytrends.interest_over_time()["İstanbul"]
reference = 0
for i in dgrs:
    reference += i
for obj in json_obj:
    if obj != "İstanbul":
        pytrends.build_payload([obj, "İstanbul"], cat=0, timeframe='today 12-m', geo='TR')
        dgrs = pytrends.interest_over_time()[obj]
        dgrs_g = 0
        for i in dgrs:
            if i == 0: i = 1
            dgrs_g += i
        if 5/(reference/dgrs_g) >= 2.5: dgrs_g = 4.5
        elif 5/(reference/dgrs_g) >= 2.3: dgrs_g = 4.0
        elif 5/(reference/dgrs_g) >= 2.0: dgrs_g = 3.5
        elif 5/(reference/dgrs_g) >= 1.7: dgrs_g = 3.0
        elif 5/(reference/dgrs_g) >= 1.5: dgrs_g = 2.5
        elif 5/(reference/dgrs_g) >= 1.0: dgrs_g = 2
        elif 5/(reference/dgrs_g) >= 0.7: dgrs_g = 1.5
        elif 5/(reference/dgrs_g) >= 0.5: dgrs_g = 1
        else: dgrs_g = 0.5
        json_obj[obj]["trendlik"] = dgrs_g
    else: json_obj[obj]["trendlik"] = 5
###################################################################################################################################


###################################################İllere popülerlikleri ekleniyor#################################################
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome(executable_path="path")
driver.get("https://thoooth.com/il-bazli-turist-sayilari-ve-yogunluklari-2018kis/")
driver.maximize_window()
time.sleep(1)
iller = []
turistler = []
def fun():
    for il in driver.find_elements(By.CLASS_NAME, "column-1")[1:]: iller.append(str(il.text))
    for turist in driver.find_elements(By.CLASS_NAME, "column-4")[1:]: turistler.append(str(turist.text))
    driver.find_element(By.XPATH, "/html/body/div/div[2]/div/main/article/div[2]/div/div[1]/div[2]/a[2]").click()
counter = 0
while counter !=8:
    fun()
    counter +=1
for il in driver.find_elements(By.CLASS_NAME, "column-1")[1:]: iller.append(str(il.text))
for turist in driver.find_elements(By.CLASS_NAME, "column-4")[1:]: turistler.append(str(turist.text))
driver.get("https://thoooth.com/il-bazli-turist-sayilari-ve-yogunluklari-2018yaz/")
driver.maximize_window()
time.sleep(1)
iller2 = []
turistler2 = []
def fun():
    for il in driver.find_elements(By.CLASS_NAME, "column-1")[1:]: iller2.append(str(il.text))
    for turist in driver.find_elements(By.CLASS_NAME, "column-4")[1:]: turistler2.append(str(turist.text))
    driver.find_element(By.XPATH, "/html/body/div/div[2]/div/main/article/div[2]/div/div[1]/div[2]/a[2]").click()
counter = 0
while counter !=8:
    fun()
    counter +=1
for il in driver.find_elements(By.CLASS_NAME, "column-1")[1:]: iller2.append(str(il.text))
for turist in driver.find_elements(By.CLASS_NAME, "column-4")[1:]: turistler2.append(str(turist.text))
driver.close()
list1 = []
counter = 0
for il in iller:
    list1.append([il, turistler[counter]])
    counter += 1
counter = 0
list2 = []
for eleman in list1: list2.append([eleman[0], int(eleman[1].replace(".", ""))+int(turistler2[iller2.index(eleman[0])].replace(".", ""))])

for i in list2:
    if i[0] == "Hakkari": i[0] = "Hakkâri"
    json_obj[i[0]]["popularite"] = float(i[1]) / 111550.88
###################################################################################################################################

####################################################İllere cami sayısı ekleniyor###################################################
html = requests.get("https://www.ensonhaber.com/gundem/turkiye-genelinde-il-il-cami-sayilari").content
soup=BeautifulSoup(html,"html.parser")
a = soup.find("article", {"class":"body"}).find_all("p")[-2].text
cami_s = []
counter = 0
while counter < 81:
    if counter == 80:
        b = [a.index("o"), a.index(" ")]
        cami_s.append([a[b[1]+1:b[0]-1], a[:b[1]]])
        a = a[b[0]+2:]
    else:
        b = [a.index(","), a.index(" ")]
        cami_s.append([a[b[1]+1:b[0]], a[:b[1]]])
        a = a[b[0]+2:]
    counter += 1
for cami in cami_s:
    if cami[1] == "Hakkari": cami[1] = "Hakkâri"
    json_obj[cami[1]]["mekan_sayisi"]["cami"] = cami[0]
###################################################################################################################################


####################################################İllere kilise sayısı ekleniyor###################################################
""" html = requests.get("https://yeniadana.net/haber/-13907.html").content
soup=BeautifulSoup(html,"html.parser") """
###################################################################################################################################


#################################################İllere ilçe sayısı felan ekleniyor################################################
html = requests.get("http://yerelnet.org.tr/iller/").content
soup=BeautifulSoup(html,"html.parser")
iller = soup.find("tbody").find_all("tr")[1:]
for il in iller:
    a = il.find_all("td")[1].text.capitalize().replace("i̇", "i").replace("İ", "İ")
    try:
        if (a == "Balikesir"): a = "Balıkesir"
        elif (a == "Diyarbakir"): a = "Diyarbakır"
        elif (a == "Hakkari"): a = "Hakkâri"
        elif (a == "Kirklareli"): a = "Kırklareli"
        elif (a == "Kirşehir"): a = "Kırşehir"
        json_obj[a]["ilce"] = il.find_all("td")[2].text
        json_obj[a]["belediye"] = il.find_all("td")[3].text
        json_obj[a]["koy"] = il.find_all("td")[4].text
    except:
        a = a.replace("i", "ı")
        json_obj[a]["ilce"] = il.find_all("td")[2].text
        json_obj[a]["belediye"] = il.find_all("td")[3].text
        json_obj[a]["koy"] = il.find_all("td")[4].text
###################################################################################################################################

###############################################json_obj main.json dosyasına yazılıyor##############################################
json_obj  = json.dumps(json_obj, ensure_ascii=False)
with open("main.json", "w", encoding="utf-8") as main:
    main.write(json_obj)
###################################################################################################################################